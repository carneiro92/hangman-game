
let lettreTrouve = null;
let lFound = 0;
playM();
function playM() {
    var word = ["Ain", "Aisne", "Allier", "Alpes-de-Haute-Provence", "Hautes-Alpes", "Alpes-Maritimes", "Ardeche", "Ardennes", "Ariege", "Aube", "Aude", "Aveyron", "Bouches-du-Rhone", "Calvados", "Cantal", "Charente", "Charente-Maritime", "Cher", "Correze", "Cote-d'Or", "Cotes-d'Armor", "Creuse", "Dordogne", "Doubs", "Drome", "Eure", "Eure-et-Loir", "Finistere", "Corse-du-Sud", "Haute-Corse", "Gard", "Haute-Garonne", "Gers", "Gironde", "Herault", "Ille-et-Vilaine", "Indre", "Indre-et-Loire", "Isere", "Jura", "Landes", "Loir-et-Cher", "Loire", "Haute-Loire", "Loire-Atlantique", "Loiret", "Lot", "Lot-et-Garonne", "Lozere", "Maine-et-Loire", "Manche", "Marne", "Haute-Marne", "Mayenne", "Meurthe-et-Moselle", "Meuse", "Morbihan", "Moselle", "Nievre", "Nord", "Oise", "Orne", "Pas-de-Calais", "Puy-de-Dome", "Pyrenees-Atlantiques", "Hautes-Pyrenees", "Pyrenees-Orientales", "Bas-Rhin", "Haut-Rhin", "Rhone", "Haute-Saone", "Saone-et-Loire", "Sarthe", "Savoie", "Haute-Savoie", "Paris", "Seine-Maritime", "Seine-et-Marne", "Yvelines", "Deux-Sevres", "Somme", "Tarn", "Tarn-et-Garonne", "Var", "Vaucluse", "Vendee", "Vienne", "Haute-Vienne", "Vosges", "Yonne", "Territoire de Belfort", "Essonne", "Hauts-de-Seine", "Seine-Saint-Denis", "Val-de-Marne", "Val-d'Oise", " Guadeloupe", " Martinique", " Guyane", "Reunion"];
    var body = document.querySelector('body');
    var score = document.querySelector("#scoreBoard");
    var choiceWord = createWord(word);
    var resultPlay = play(word);
    var letterF = htmlGen(resultPlay);
    var keyBoardF = keyBoardGen(keyBoard);
    var letterL = choiceWord.length;
    let count = 1;
    
    ////////Declaration
    function play(wordList) {

        choiceWord = createWord(wordList); //envoie + reception balle
        var letters = splitWord(choiceWord);
        return letters;
    }

    /**
     * Prend un mot au hasard dans le tableau um mot et retoune le mot
     * @param {String} word 
     */
    function createWord(word) {
        let wordI = Math.floor(Math.random() * word.length);
        let wordG = word[wordI].toUpperCase();
        return wordG; //envoie ball
    }

    function splitWord(wordG) {
        let letter = wordG.split('');
        return letter;
    }
    /**
     * 
     * @param {String} sLetter 
     * Gère le HTML pour les espaces de le mot a trouver
     */
    function htmlGen(sLetter) {
        playground = document.getElementById('playground');
        playground.innerHTML = '';
        playground.className = "col-12"
        for (const i of sLetter) {
            span = document.createElement('span');
            letterP = document.createElement('label');
            playground.appendChild(span)
            span.appendChild(letterP);
            letterP.value = i;
            letterP.id = "L" + i;
            letterP.className = i;
            letterP.textContent = i;
            if (i === '-' || i === "'") {
                letterP.className = 'visible';
                lFound++;
            } else {
                letterP.className = 'notFound';
            }
        }
    }
    /**
     * Cette function sert a generer le clavier et l'afficher dans la page HTML
     */
    function keyBoardGen() {
        let abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let abcSplit = abc.split('');
        keysdiv = document.getElementById('keyBoard');
        keysdiv.innerHTML = "";
        for (const i of abcSplit) {
            btn = document.createElement('button');
            keysdiv.appendChild(btn);
            i.toLowerCase;
            btn.id = i;
            btn.accessKey = i;
            btn.onclick = function () { verify({ key: i }); };
            btn.className = "btn btn-primary btn-md ml-2 mb-2";
            btn.textContent = i;

        }
    }

    /**
     * Verifie si la touche ou le button appuié existe dans le mot a deviné
     * et si elle exist la ou les lettres seront affichés et au meme temps le button serà desactivé
     */
    body.addEventListener('keypress', verify);
    function verify(event) {

        let bt = document.querySelector('#' + event.key)
        bt.className = 'btn btn-primary btn-md ml-2 mb-2 disabled';
        for (const i of resultPlay) {
            if (event.key === i) {
                lettreTrouve = true;
                let letterG = document.querySelectorAll("#L" + i);
                lFound++;
                for (const i of letterG) {
                    if (i.classList.contains('notFound'))
                        i.className = 'found';
                }

            }
        }
        //Si on a trouvé une lettre on verifie si on a gagné  sinon on apelle la function change score
        if (lettreTrouve === true) {
            //winVal();
            lettreTrouve = null;
        } else {
            scoreChange(false);
        }
        if (lFound == letterL) {
            alert("Bravo t'as decouvert le mot caché!");
            window.location.reload();
            
        }
    }
    /**
     * 
     * @param {Boolean} val 
     * Dans le event listener du body au keypress la touche apuié est verifiè et si la valeur du paramètre
     * est egal a false le count sera incremente de 1
     */
    function scoreChange(val) {
        
        var pendu1 = document.getElementById('pendu');
        if (val === false) {
            count++;
        }
        if (count == 0) {
            pendu1.src = "img/pendu1.png";
        } else if (count >= 1 && count <= 7) {
            pendu1.src = "img/pendu" + count + ".png";

        }
        if (count >= 8) {
            alert('Perdu le Mot attendu etais: ' + choiceWord);
            window.location.reload();
        }
        setTimeout(winVal(),2000);
    }
    function winVal() {
        if (lFound == letterL) {
            alert("Bravo t'as decouvert le mot caché!");
            window.location.reload();
            
        }
    }
}